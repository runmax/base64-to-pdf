import React, { useState, useEffect } from "react";
import "./style.css";
import { defaultPDF } from "./defaultPDF";

function App() {
	const [data, setData] = useState(defaultPDF);
	const [pdf, setPdf] = useState("");

	useEffect(() => {
		const cleanBase64 = data
			.replace(/<\/\w+>/g, "")
			.replace(/<\w+>/g, "")
			.replace(" ", "")
			.replace("\n", "");
		setPdf("data:application/pdf;base64," + cleanBase64);
	}, [data]);

	return (
		<div className="App">
			<iframe title="pdf" src={pdf} className="viewItem" />

			<div className="viewItem">
				<textarea
					className=""
					placeholder="paste your base64 here"
					onChange={(e) => {
						setData(e.target.value);
					}}
				/>
			</div>
		</div>
	);
}

export default App;
